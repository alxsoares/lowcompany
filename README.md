Passos para testar o backend do gerenciamento de clientes da Lowcompany:

1 - Pode ser testado no Postman ou diretamento no Swagger, pois o projeto esta configurado com o Spring Swaggger.
2 - Caso teste no Swagger, acessar a URL http://localhost:9000/swagger-ui/:
	2.1 - Gerar token na api AuthenticationEndpoint, passando o login 'ADMIN' e senha 'admin123';
	2.2 - Clicar no botão "Authorize" e colocar o token da seguinte forma:
		Bearer <<token>>
	2.3 - A autenticação fica na memória do navegador, sempre que executar uma API que necessite de autenticação é inserido
	      automaticamente no cabeçalho Authorization do header; 
		  
3 - Caso teste no postman, acessar o Postman:
	1.1 - Importar a URL http://localhost:9000/v2/api-docs no Postman para gerar as request das APIs automaticamente;
	1.2 - Criar um novo ambiente no Postman com os seguintes VARIABLES:
	
		host - http://localhost:9000
		user - ADMIN
		password - admin123
		bearer_token - deixar vazio
		
    1.3 - No request de Autenticação, colocar no Header o 'Authorization = Bearer {{bearer_token}}'
	1.4 - Na aba 'Test' definir o seguinte script:
	
		if(responseCode.code >= 200 && responseCode.code <= 299){
    	var jsonData = JSON.parse(responseBody);
    	postman.setEnvironmentVariable('bearer_token', jsonData.token);
		
	1.5 - Com isso é possivel executar as APIs disponiveis;
	
	Criação e envio de email:
	
	Para que o envio de email funcione, é necessário passar um email válido no contato, pois o sistema esta enviando
	email de verdade para o email abaixo:
	
	Email: lowcompany2@gmail.com
	Senha: lowCOMPANY2
	
	Pode usar o mesmo e-mail pra teste.
	
	Obs.: Este email foi criado pra teste da funçãpo de envio, acessa-lo para ver os emails enviado.

Banco de Dados

O banco de dados da aplicação é o H2, roda embutido no spring boot, para acessa-lo, seguir as seguintes instruções:

1 - Ao subir a aplicação acessar a url http://localhost:9000/h2;
	Usuário: admin
	Senha: admin
	
Obs. Coloquei a minha base de dados em Downloads, caso for utiliza-la, as tabelas user, pemission e user_permission
já estão populadas com os dados necessário pra gerar um token.

usarname: ADMIN e password:admin123

Necessário entrar no arquivo application.properties e altera-lo:

1 - Para que seja gerado a base de dados é necessário alterar a seguinte propriedade:

	spring.datasource.url=jdbc:h2:file:~/Documents/TESTE_MOBBUY/banco_dados_h2/lowcompany-db

    Deve aponta para um caminho existente no computador executor.
	
	
Caso necessite de mais informaçoes para executar este projeto, contato através do seguinte e-mail:

alex_sandrosoares@hotmail.com

