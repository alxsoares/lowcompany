package com.lowcompany.converter;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lowcompany.converter.mocks.MockCliente;
import com.lowcompany.gerenciarcliente.converter.DozerConverter;
import com.lowcompany.gerenciarcliente.entity.Cliente;
import com.lowcompany.gerenciarcliente.entity.Contato;
import com.lowcompany.gerenciarcliente.entity.Endereco;
import com.lowcompany.gerenciarcliente.vo.ClienteVO;
import com.lowcompany.gerenciarcliente.vo.ContatoVO;
import com.lowcompany.gerenciarcliente.vo.EnderecoVO;

public class DozerConverterTest {

	MockCliente inputObject;

	@Before
	public void setUp() {
		inputObject = new MockCliente();
	}

	@Test
	public void parseEntityToVOTest() {
		ClienteVO output = DozerConverter.parseObject(inputObject.mockEntity(), ClienteVO.class);

		Assert.assertEquals(Long.valueOf(1L), output.getId());
		Assert.assertEquals("Joao1", output.getNome());

		ContatoVO contatoVO = output.getContatos().iterator().next();

		Assert.assertEquals("Joao da Siva1", contatoVO.getNome());
		Assert.assertEquals("joao@lowcompany.com1", contatoVO.getEmail());
		Assert.assertEquals("CEL1", contatoVO.getTipoTelefone());
		Assert.assertEquals("998786781", contatoVO.getNumeroTelefone());

		EnderecoVO endereco = output.getEndereco();

		Assert.assertEquals("R Marmore1", endereco.getRua());
		Assert.assertEquals(Integer.valueOf(34), endereco.getNumero());
		Assert.assertEquals("São Paulo1", endereco.getCidade());
		Assert.assertEquals("SP1", endereco.getUf());
		Assert.assertEquals("058542701", endereco.getCep());
		Assert.assertEquals("Proximo Escola1", endereco.getComplemento());

		Assert.assertEquals("555666777881", output.getNumeroDocumentoCPF());
		Assert.assertEquals("444444444444441", output.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(output.getDataCadastro());
		Assert.assertNotNull(output.getDataUltimaAlteracao());
	}

	@Test
	public void parseEntityListToVOListTest() {
		List<ClienteVO> outputList = DozerConverter.parseListObjects(inputObject.mockEntityList(), ClienteVO.class);

		ClienteVO outputZero = outputList.get(0);

		Assert.assertEquals(Long.valueOf(1L), outputZero.getId());
		Assert.assertEquals("Joao1", outputZero.getNome());

		ContatoVO contatoZero = outputZero.getContatos().iterator().next();

		Assert.assertEquals("Joao da Siva1", contatoZero.getNome());
		Assert.assertEquals("joao@lowcompany.com1", contatoZero.getEmail());
		Assert.assertEquals("CEL1", contatoZero.getTipoTelefone());
		Assert.assertEquals("998786781", contatoZero.getNumeroTelefone());

		EnderecoVO enderecoZero = outputZero.getEndereco();

		Assert.assertEquals("R Marmore1", enderecoZero.getRua());
		Assert.assertEquals(Integer.valueOf(34), enderecoZero.getNumero());
		Assert.assertEquals("São Paulo1", enderecoZero.getCidade());
		Assert.assertEquals("SP1", enderecoZero.getUf());
		Assert.assertEquals("058542701", enderecoZero.getCep());
		Assert.assertEquals("Proximo Escola1", enderecoZero.getComplemento());

		Assert.assertEquals("555666777881", outputZero.getNumeroDocumentoCPF());
		Assert.assertEquals("444444444444441", outputZero.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(outputZero.getDataCadastro());
		Assert.assertNotNull(outputZero.getDataUltimaAlteracao());

		ClienteVO outputSete = outputList.get(7);

		Assert.assertEquals(Long.valueOf(8L), outputSete.getId());
		Assert.assertEquals("Joao8", outputSete.getNome());

		ContatoVO contatoVO = outputSete.getContatos().iterator().next();

		Assert.assertEquals("Joao da Siva8", contatoVO.getNome());
		Assert.assertEquals("joao@lowcompany.com8", contatoVO.getEmail());
		Assert.assertEquals("CEL8", contatoVO.getTipoTelefone());
		Assert.assertEquals("998786788", contatoVO.getNumeroTelefone());

		EnderecoVO enderecoSete = outputSete.getEndereco();

		Assert.assertEquals("R Marmore8", enderecoSete.getRua());
		Assert.assertEquals(Integer.valueOf(34), enderecoSete.getNumero());
		Assert.assertEquals("São Paulo8", enderecoSete.getCidade());
		Assert.assertEquals("SP8", enderecoSete.getUf());
		Assert.assertEquals("058542708", enderecoSete.getCep());
		Assert.assertEquals("Proximo Escola8", enderecoSete.getComplemento());

		Assert.assertEquals("555666777888", outputSete.getNumeroDocumentoCPF());
		Assert.assertEquals("444444444444448", outputSete.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(outputSete.getDataCadastro());
		Assert.assertNotNull(outputSete.getDataUltimaAlteracao());

		ClienteVO outputDoze = outputList.get(12);

		Assert.assertEquals(Long.valueOf(13L), outputDoze.getId());
		Assert.assertEquals("Joao13", outputDoze.getNome());

		ContatoVO contatoDoze = outputDoze.getContatos().iterator().next();

		Assert.assertEquals("Joao da Siva13", contatoDoze.getNome());
		Assert.assertEquals("joao@lowcompany.com13", contatoDoze.getEmail());
		Assert.assertEquals("CEL13", contatoDoze.getTipoTelefone());
		Assert.assertEquals("9987867813", contatoDoze.getNumeroTelefone());

		EnderecoVO enderecoDoze = outputDoze.getEndereco();

		Assert.assertEquals("R Marmore13", enderecoDoze.getRua());
		Assert.assertEquals(Integer.valueOf(34), enderecoDoze.getNumero());
		Assert.assertEquals("São Paulo13", enderecoDoze.getCidade());
		Assert.assertEquals("SP13", enderecoDoze.getUf());
		Assert.assertEquals("0585427013", enderecoDoze.getCep());
		Assert.assertEquals("Proximo Escola13", enderecoDoze.getComplemento());

		Assert.assertEquals("5556667778813", outputDoze.getNumeroDocumentoCPF());
		Assert.assertEquals("4444444444444413", outputDoze.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(outputDoze.getDataCadastro());
		Assert.assertNotNull(outputDoze.getDataUltimaAlteracao());
	}

	@Test
	public void parseVOToEntityTest() {
		Cliente output = DozerConverter.parseObject(inputObject.mockEntityVO(), Cliente.class);

		Assert.assertEquals(Long.valueOf(1L), output.getId());
		Assert.assertEquals("Fred1", output.getNome());

		Contato contato = output.getContatos().iterator().next();

		Assert.assertEquals("Fred Santos1", contato.getNome());
		Assert.assertEquals("fred@lowcompany.com1", contato.getEmail());
		Assert.assertEquals("CEL1", contato.getTipoTelefone());
		Assert.assertEquals("9453478961", contato.getNumeroTelefone());

		Endereco endereco = output.getEndereco();

		Assert.assertEquals("Av Jacuru1", endereco.getRua());
		Assert.assertEquals(Integer.valueOf(987), endereco.getNumero());
		Assert.assertEquals("Rio de Janeiro1", endereco.getCidade());
		Assert.assertEquals("RJ1", endereco.getUf());
		Assert.assertEquals("233343321", endereco.getCep());
		Assert.assertEquals("Proximo UBS1", endereco.getComplemento());

		Assert.assertEquals("777777777771", output.getNumeroDocumentoCPF());
		Assert.assertEquals("121212212122121", output.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(output.getDataCadastro());
		Assert.assertNotNull(output.getDataUltimaAlteracao());
	}

	@Test
	public void parserVOListToEntityListTest() {
		List<Cliente> outputList = DozerConverter.parseListObjects(inputObject.mockEntityListVO(), Cliente.class);

		Cliente outputZero = outputList.get(0);

		Assert.assertEquals(Long.valueOf(1L), outputZero.getId());
		Assert.assertEquals("Fred1", outputZero.getNome());

		Contato contatoZero = outputZero.getContatos().iterator().next();

		Assert.assertEquals("Fred Santos1", contatoZero.getNome());
		Assert.assertEquals("fred@lowcompany.com1", contatoZero.getEmail());
		Assert.assertEquals("CEL1", contatoZero.getTipoTelefone());
		Assert.assertEquals("9453478961", contatoZero.getNumeroTelefone());

		Endereco enderecoZero = outputZero.getEndereco();

		Assert.assertEquals("Av Jacuru1", enderecoZero.getRua());
		Assert.assertEquals(Integer.valueOf(987), enderecoZero.getNumero());
		Assert.assertEquals("Rio de Janeiro1", enderecoZero.getCidade());
		Assert.assertEquals("RJ1", enderecoZero.getUf());
		Assert.assertEquals("233343321", enderecoZero.getCep());
		Assert.assertEquals("Proximo UBS1", enderecoZero.getComplemento());

		Assert.assertEquals("777777777771", outputZero.getNumeroDocumentoCPF());
		Assert.assertEquals("121212212122121", outputZero.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(outputZero.getDataCadastro());
		Assert.assertNotNull(outputZero.getDataUltimaAlteracao());

		Cliente outputSeven = outputList.get(7);

		Assert.assertEquals(Long.valueOf(8L), outputSeven.getId());
		Assert.assertEquals("Fred8", outputSeven.getNome());

		Contato contatoSete = outputSeven.getContatos().iterator().next();

		Assert.assertEquals("Fred Santos8", contatoSete.getNome());
		Assert.assertEquals("fred@lowcompany.com8", contatoSete.getEmail());
		Assert.assertEquals("CEL8", contatoSete.getTipoTelefone());
		Assert.assertEquals("9453478968", contatoSete.getNumeroTelefone());

		Endereco enderecoSete = outputSeven.getEndereco();

		Assert.assertEquals("Av Jacuru8", enderecoSete.getRua());
		Assert.assertEquals(Integer.valueOf(987), enderecoSete.getNumero());
		Assert.assertEquals("Rio de Janeiro8", enderecoSete.getCidade());
		Assert.assertEquals("RJ8", enderecoSete.getUf());
		Assert.assertEquals("233343328", enderecoSete.getCep());
		Assert.assertEquals("Proximo UBS8", enderecoSete.getComplemento());

		Assert.assertEquals("777777777778", outputSeven.getNumeroDocumentoCPF());
		Assert.assertEquals("121212212122128", outputSeven.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(outputSeven.getDataCadastro());
		Assert.assertNotNull(outputSeven.getDataUltimaAlteracao());

		Cliente outputTwelve = outputList.get(12);

		Assert.assertEquals(Long.valueOf(13L), outputTwelve.getId());
		Assert.assertEquals("Fred13", outputTwelve.getNome());

		Contato contatoDoze = outputTwelve.getContatos().iterator().next();

		Assert.assertEquals("Fred Santos13", contatoDoze.getNome());
		Assert.assertEquals("fred@lowcompany.com13", contatoDoze.getEmail());
		Assert.assertEquals("CEL13", contatoDoze.getTipoTelefone());
		Assert.assertEquals("94534789613", contatoDoze.getNumeroTelefone());

		Endereco enderecoDoze = outputTwelve.getEndereco();

		Assert.assertEquals("Av Jacuru13", enderecoDoze.getRua());
		Assert.assertEquals(Integer.valueOf(987), enderecoDoze.getNumero());
		Assert.assertEquals("Rio de Janeiro13", enderecoDoze.getCidade());
		Assert.assertEquals("RJ13", enderecoDoze.getUf());
		Assert.assertEquals("2333433213", enderecoDoze.getCep());
		Assert.assertEquals("Proximo UBS13", enderecoDoze.getComplemento());

		Assert.assertEquals("7777777777713", outputTwelve.getNumeroDocumentoCPF());
		Assert.assertEquals("1212122121221213", outputTwelve.getNumeroDocumentoCNPJ());
		Assert.assertNotNull(outputTwelve.getDataCadastro());
		Assert.assertNotNull(outputTwelve.getDataUltimaAlteracao());
	}
}