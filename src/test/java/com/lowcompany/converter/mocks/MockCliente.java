package com.lowcompany.converter.mocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.lowcompany.gerenciarcliente.entity.Cliente;
import com.lowcompany.gerenciarcliente.entity.Contato;
import com.lowcompany.gerenciarcliente.entity.Endereco;
import com.lowcompany.gerenciarcliente.vo.ClienteVO;
import com.lowcompany.gerenciarcliente.vo.ContatoVO;
import com.lowcompany.gerenciarcliente.vo.EnderecoVO;

public class MockCliente {

	public Cliente mockEntity() {
		return mockEntity(1L);
	}

	public ClienteVO mockEntityVO() {
		return mockEntityVO(1L);
	}

	public List<Cliente> mockEntityList() {
		List<Cliente> entities = new ArrayList<Cliente>();

		for (long i = 1; i <= 14; i++) {
			entities.add(mockEntity(i));
		}
		return entities;
	}

	public List<ClienteVO> mockEntityListVO() {
		List<ClienteVO> entities = new ArrayList<ClienteVO>();

		for (long i = 1; i <= 14; i++) {
			entities.add(mockEntityVO(i));
		}
		return entities;
	}

	private Cliente mockEntity(Long i) {
		Date now = new Date();
		
		Cliente cliente = new Cliente();
		cliente.setId(i);
		cliente.setNome("Joao" + i);
		
		Contato contato = new Contato();
		contato.setId(i);
		contato.setNome("Joao da Siva" + i);
		contato.setEmail("joao@lowcompany.com" + i);
		contato.setNumeroTelefone("99878678" + i);
		contato.setTipoTelefone("CEL" + i);
		
		Set<Contato> contatos = new HashSet<Contato>();
		contatos.add(contato);
		
		Endereco endereco = new Endereco();
		endereco.setId(i);
		endereco.setRua("R Marmore" + i);
		endereco.setNumero(34);
		endereco.setCidade("São Paulo" + i);
		endereco.setUf("SP" + i);
		endereco.setComplemento("Proximo Escola" + i);
		endereco.setCep("05854270" + i);
		
		cliente.setContatos(contatos);
		cliente.setEndereco(endereco);
		cliente.setNumeroDocumentoCPF("55566677788" + i);
		cliente.setNumeroDocumentoCNPJ("44444444444444" + i);
		cliente.setUrlSite("lowcompany.com" + i);
		cliente.setDataCadastro(now);
		cliente.setDataUltimaAlteracao(now);
		return cliente;
	}

	private ClienteVO mockEntityVO(Long i) {
		Date now = new Date();
		
		ClienteVO cliente = new ClienteVO();
		cliente.setId(i);
		cliente.setNome("Fred" + i);
		
		Set<ContatoVO> contatos = new HashSet<ContatoVO>();
		
		ContatoVO contato = new ContatoVO();
		contato.setId(i);
		contato.setNome("Fred Santos" + i);
		contato.setEmail("fred@lowcompany.com" + i);
		contato.setNumeroTelefone("945347896" + i);
		contato.setTipoTelefone("CEL" + i);
				
		contatos.add(contato);
		
		EnderecoVO endereco = new EnderecoVO();
		endereco.setId(i);
		endereco.setRua("Av Jacuru" + i);
		endereco.setNumero(987);
		endereco.setCidade("Rio de Janeiro" + i);
		endereco.setUf("RJ" + i);
		endereco.setComplemento("Proximo UBS" + i);
		endereco.setCep("23334332" + i);
		
		cliente.setContatos(contatos);
		cliente.setEndereco(endereco);
		cliente.setNumeroDocumentoCPF("77777777777" + i);
		cliente.setNumeroDocumentoCNPJ("12121221212212" + i);
		cliente.setUrlSite("lowcompany.com" + i);
		cliente.setDataCadastro(now);
		cliente.setDataUltimaAlteracao(now);
		
		return cliente;
	}
}
