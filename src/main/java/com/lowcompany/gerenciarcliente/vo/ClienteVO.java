package com.lowcompany.gerenciarcliente.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClienteVO extends PessoaVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	@JsonProperty("numero_documento_cnpj")
	private String numeroDocumentoCNPJ;
	@JsonProperty("url_site")
	private String urlSite;
	@JsonIgnore
	private Date dataCadastro;
	@JsonIgnore
	private Date dataUltimaAlteracao;
	private Set<ContatoVO> contatos;

	public ClienteVO() {}
	
	public ClienteVO(String nome, String numeroDocumentoCPF) {
		super(nome, numeroDocumentoCPF);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the numeroDocumentoCNPJ
	 */
	public String getNumeroDocumentoCNPJ() {
		return numeroDocumentoCNPJ;
	}

	/**
	 * @param numeroDocumentoCNPJ the numeroDocumentoCNPJ to set
	 */
	public void setNumeroDocumentoCNPJ(String numeroDocumentoCNPJ) {
		this.numeroDocumentoCNPJ = numeroDocumentoCNPJ;
	}

	/**
	 * @return the urlSite
	 */
	public String getUrlSite() {
		return urlSite;
	}

	/**
	 * @param urlSite the urlSite to set
	 */
	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	/**
	 * @return the dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}

	/**
	 * @param dataCadastro the dataCadastro to set
	 */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * @return the dataUltimaAlteracao
	 */
	public Date getDataUltimaAlteracao() {
		return dataUltimaAlteracao;
	}

	/**
	 * @param dataUltimaAlteracao the dataUltimaAlteracao to set
	 */
	public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
		this.dataUltimaAlteracao = dataUltimaAlteracao;
	}

	/**
	 * @return the contatos
	 */
	public Set<ContatoVO> getContatos() {
		return contatos;
	}

	/**
	 * @param contatos the contatos to set
	 */
	public void setContatos(Set<ContatoVO> contatos) {
		this.contatos = contatos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((contatos == null) ? 0 : contatos.hashCode());
		result = prime * result + ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result + ((dataUltimaAlteracao == null) ? 0 : dataUltimaAlteracao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((numeroDocumentoCNPJ == null) ? 0 : numeroDocumentoCNPJ.hashCode());
		result = prime * result + ((urlSite == null) ? 0 : urlSite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteVO other = (ClienteVO) obj;
		if (contatos == null) {
			if (other.contatos != null)
				return false;
		} else if (!contatos.equals(other.contatos))
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (dataUltimaAlteracao == null) {
			if (other.dataUltimaAlteracao != null)
				return false;
		} else if (!dataUltimaAlteracao.equals(other.dataUltimaAlteracao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numeroDocumentoCNPJ == null) {
			if (other.numeroDocumentoCNPJ != null)
				return false;
		} else if (!numeroDocumentoCNPJ.equals(other.numeroDocumentoCNPJ))
			return false;
		if (urlSite == null) {
			if (other.urlSite != null)
				return false;
		} else if (!urlSite.equals(other.urlSite))
			return false;
		return true;
	}
}
