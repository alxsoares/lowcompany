package com.lowcompany.gerenciarcliente.vo;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class PessoaVO extends RepresentationModel<PessoaVO> {

	private String nome;
	@JsonProperty("numero_documento_cpf")
	private String numeroDocumentoCPF;
	private EnderecoVO endereco;

	public PessoaVO() {}
	
	public PessoaVO(String nome, String numeroDocumentoCPF) {
		this.nome = nome;
		this.numeroDocumentoCPF = numeroDocumentoCPF;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the numeroDocumentoCPF
	 */
	public String getNumeroDocumentoCPF() {
		return numeroDocumentoCPF;
	}

	/**
	 * @param numeroDocumentoCPF the numeroDocumentoCPF to set
	 */
	public void setNumeroDocumentoCPF(String numeroDocumentoCPF) {
		this.numeroDocumentoCPF = numeroDocumentoCPF;
	}

	/**
	 * @return the endereco
	 */
	public EnderecoVO getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(EnderecoVO endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numeroDocumentoCPF == null) ? 0 : numeroDocumentoCPF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaVO other = (PessoaVO) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numeroDocumentoCPF == null) {
			if (other.numeroDocumentoCPF != null)
				return false;
		} else if (!numeroDocumentoCPF.equals(other.numeroDocumentoCPF))
			return false;
		return true;
	}
}
