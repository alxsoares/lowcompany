package com.lowcompany.gerenciarcliente.config;

import static io.swagger.models.auth.In.HEADER;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final String DEFAULT_INCLUDE_PATTERN = "/api/*";
    
	@Bean
	public Docket jsonApi() {
	    return new Docket(SWAGGER_2)
	        .securitySchemes(singletonList(new ApiKey("JWT", AUTHORIZATION, HEADER.name())))
	        .securityContexts(singletonList(
	            SecurityContext.builder()
	                .securityReferences(
	                    singletonList(SecurityReference.builder()
	                        .reference("JWT")
	                        .scopes(new AuthorizationScope[0])
	                        .build()
	                    )
	                )
	                .build())
	        )
	        .select()
	        .apis(RequestHandlerSelectors.any())
	        .build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("Gerenciamento de clientes", "Permite que a empresa LowCompany gerencie seus clientes", "V1", "terms Of Service Url",
				new Contact("LowCompany", "www.lowcompany.com", "lowcompany@lowcompany.com"), "Licença da API",
				"Licença da URL", Collections.emptyList());
	}
}
