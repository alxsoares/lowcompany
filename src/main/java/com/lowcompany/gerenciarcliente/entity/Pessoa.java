package com.lowcompany.gerenciarcliente.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass 
public abstract class Pessoa {

	@Column(name = "nome", nullable = false)
	private String nome;
	@Column(name = "numero_documento_cpf" , unique = true, length = 11, nullable = false)
	private String numeroDocumentoCPF;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_endereco", referencedColumnName = "id")
	private Endereco endereco;

	public Pessoa() {}
	
	public Pessoa(String nome, String numeroDocumentoCPF) {
		this.nome = nome;
		this.numeroDocumentoCPF = numeroDocumentoCPF;
	}
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the numeroDocumentoCPF
	 */
	public String getNumeroDocumentoCPF() {
		return numeroDocumentoCPF;
	}

	/**
	 * @param numeroDocumentoCPF the numeroDocumentoCPF to set
	 */
	public void setNumeroDocumentoCPF(String numeroDocumentoCPF) {
		this.numeroDocumentoCPF = numeroDocumentoCPF;
	}

	/**
	 * @return the endereco
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numeroDocumentoCPF == null) ? 0 : numeroDocumentoCPF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numeroDocumentoCPF == null) {
			if (other.numeroDocumentoCPF != null)
				return false;
		} else if (!numeroDocumentoCPF.equals(other.numeroDocumentoCPF))
			return false;
		return true;
	}
}
