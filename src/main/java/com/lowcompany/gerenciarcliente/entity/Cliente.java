package com.lowcompany.gerenciarcliente.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Entity
@Table(name = "cliente")
public class Cliente extends Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "numero_documento_cnpj", unique = true, length = 14)
	private String numeroDocumentoCNPJ;
	@Column(name = "url_site")
	private String urlSite;
	@Column(name = "data_cadastro", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	@Column(name = "data_ultima_alteracao", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataUltimaAlteracao;
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(name = "cliente_contato", joinColumns = {
			@JoinColumn(columnDefinition = "id_cliente") }, inverseJoinColumns = {
					@JoinColumn(columnDefinition = "id_contato") })
	private Set<Contato> contatos;
	
	public Cliente() {}
	
	public Cliente(String nome, String numeroDocumentoCPF) {
		super(nome, numeroDocumentoCPF);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the numeroDocumentoCNPJ
	 */
	public String getNumeroDocumentoCNPJ() {
		return numeroDocumentoCNPJ;
	}

	/**
	 * @param numeroDocumentoCNPJ the numeroDocumentoCNPJ to set
	 */
	public void setNumeroDocumentoCNPJ(String numeroDocumentoCNPJ) {
		this.numeroDocumentoCNPJ = numeroDocumentoCNPJ;
	}

	/**
	 * @return the urlSite
	 */
	public String getUrlSite() {
		return urlSite;
	}

	/**
	 * @param urlSite the urlSite to set
	 */
	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	/**
	 * @return the dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}

	/**
	 * @param dataCadastro the dataCadastro to set
	 */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * @return the dataUltimaAlteracao
	 */
	public Date getDataUltimaAlteracao() {
		return dataUltimaAlteracao;
	}

	/**
	 * @param dataUltimaAlteracao the dataUltimaAlteracao to set
	 */
	public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
		this.dataUltimaAlteracao = dataUltimaAlteracao;
	}

	/**
	 * @return the contatos
	 */
	public Set<Contato> getContatos() {
		return contatos;
	}

	/**
	 * @param contatos the contatos to set
	 */
	public void setContatos(Set<Contato> contatos) {
		this.contatos = contatos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contatos == null) ? 0 : contatos.hashCode());
		result = prime * result + ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result + ((dataUltimaAlteracao == null) ? 0 : dataUltimaAlteracao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((numeroDocumentoCNPJ == null) ? 0 : numeroDocumentoCNPJ.hashCode());
		result = prime * result + ((urlSite == null) ? 0 : urlSite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (contatos == null) {
			if (other.contatos != null)
				return false;
		} else if (!contatos.equals(other.contatos))
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (dataUltimaAlteracao == null) {
			if (other.dataUltimaAlteracao != null)
				return false;
		} else if (!dataUltimaAlteracao.equals(other.dataUltimaAlteracao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numeroDocumentoCNPJ == null) {
			if (other.numeroDocumentoCNPJ != null)
				return false;
		} else if (!numeroDocumentoCNPJ.equals(other.numeroDocumentoCNPJ))
			return false;
		if (urlSite == null) {
			if (other.urlSite != null)
				return false;
		} else if (!urlSite.equals(other.urlSite))
			return false;
		return true;
	}
}
