package com.lowcompany.gerenciarcliente.helper;

import org.apache.logging.log4j.util.Strings;

import com.lowcompany.gerenciarcliente.util.Validator;
import com.lowcompany.gerenciarcliente.vo.ClienteVO;
import com.lowcompany.gerenciarcliente.vo.ContatoVO;

public class ValidateFieldsHelper {

	public static boolean validateFields(ClienteVO vo) {

		boolean docCPFValidado = false;
		boolean docCNPJValidado = false;
		boolean emailValido = false;

		if (Strings.isNotBlank(vo.getNumeroDocumentoCPF()) && Validator.isValidCPF(vo.getNumeroDocumentoCPF())) {
			docCPFValidado = true;
		} 
		
		if (Strings.isNotBlank(vo.getNumeroDocumentoCNPJ()) && Validator.isValidCNPJ(vo.getNumeroDocumentoCNPJ())) {
			docCNPJValidado = true;
		} 

		if(vo.getContatos() != null) {
			for(ContatoVO contato : vo.getContatos()) {
				if(!Validator.isValidEmailAddress(contato.getEmail())) {
					emailValido = false;
					break;
				}else {
					emailValido = true;
				}
			}
		}
		
		return docCPFValidado && emailValido && docCNPJValidado;
	}
}
