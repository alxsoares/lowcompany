package com.lowcompany.gerenciarcliente.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lowcompany.gerenciarcliente.vo.ClienteVO;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
public interface ClienteService {
	
	ClienteVO create(ClienteVO vo);

	Page<ClienteVO> findAll(Pageable pageable);

	ClienteVO getById(Long id);

	void deleteById(Long id);

	ClienteVO update(ClienteVO vo);

}
