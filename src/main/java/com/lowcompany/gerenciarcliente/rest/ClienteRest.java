package com.lowcompany.gerenciarcliente.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lowcompany.gerenciarcliente.helper.ValidateFieldsHelper;
import com.lowcompany.gerenciarcliente.interfaces.ClienteService;
import com.lowcompany.gerenciarcliente.vo.ClienteVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Api(value = "Cliente Endpoint", description = "Gerenciar Clientes", tags = "ClienteEndpoint")
@RestController
@RequestMapping(path = "/api/cliente/v1")
public class ClienteRest {

	@Autowired
	private ClienteService clienteService;
	@Autowired
	private PagedResourcesAssembler<ClienteVO> assembler;

	@ApiOperation(value = "Adiciona um cliente na base de dados")
	@PostMapping(produces = { "application/json", "application/xml", "application/x-yaml" }, consumes = {
			"application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<?> create(@RequestBody ClienteVO vo) {

		if (!ValidateFieldsHelper.validateFields(vo)) {
			return new ResponseEntity<>("CPF/CNPJ/Email inválido", HttpStatus.NO_CONTENT);
		} else {
			ClienteVO cliente = this.clienteService.create(vo);
			cliente.add(linkTo(methodOn(ClienteRest.class).create(vo)).withSelfRel());
			return new ResponseEntity<>(cliente, HttpStatus.CREATED);
		}
	}

	@ApiOperation(value = "Recupera um cliente por ID")
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		ClienteVO vo = this.clienteService.getById(id);

		if (vo == null) {
			return new ResponseEntity<>(vo, HttpStatus.NOT_FOUND);
		} else {
			vo.add(linkTo(methodOn(ClienteRest.class).getById(id)).withSelfRel());
			return new ResponseEntity<>(vo, HttpStatus.OK);

		}
	}

	@ApiOperation(value = "Recupera todos os clientes")
	@GetMapping(produces = { "application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<?> findAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "12") int limit,
			@RequestParam(value = "direction", defaultValue = "asc") String direction) {

		var sortDirection = "desc".equalsIgnoreCase(direction) ? Direction.DESC : Direction.ASC;

		Pageable pageable = PageRequest.of(page, limit, Sort.by(sortDirection, "nome"));

		Page<ClienteVO> clientes = this.clienteService.findAll(pageable);

		clientes.stream().forEach(cliente -> cliente
				.add(linkTo(methodOn(ClienteRest.class).findAll(page, limit, direction)).withSelfRel()));

		PagedModel<?> model = assembler.toModel(clientes);

		return new ResponseEntity<>(model, HttpStatus.OK);
	}

	@ApiOperation(value = "Deleta um cliente na base de dados por ID")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
		this.clienteService.deleteById(id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Atualiza um cliente na base de dados")
	@PutMapping(produces = { "application/json", "application/xml", "application/x-yaml" }, consumes = {
			"application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<?> update(@RequestBody ClienteVO vo) {

		if (ValidateFieldsHelper.validateFields(vo)) {
			ClienteVO cliente = this.clienteService.update(vo);
			cliente.add(linkTo(methodOn(ClienteRest.class).update(vo)).withSelfRel());
			return new ResponseEntity<>(cliente, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("CPF/CNPJ/Email inválido", HttpStatus.NO_CONTENT);
		}
	}	
}
