package com.lowcompany.gerenciarcliente.service;

import java.util.Date;
import java.util.Optional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lowcompany.gerenciarcliente.converter.DozerConverter;
import com.lowcompany.gerenciarcliente.entity.Cliente;
import com.lowcompany.gerenciarcliente.exception.ResourceNotFoundException;
import com.lowcompany.gerenciarcliente.interfaces.ClienteService;
import com.lowcompany.gerenciarcliente.repository.ClienteRepositoryImpl;
import com.lowcompany.gerenciarcliente.util.JavaMail;
import com.lowcompany.gerenciarcliente.vo.ClienteVO;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepositoryImpl repository;

	public ClienteVO create(ClienteVO vo) {
		
		vo.setDataCadastro(new Date());
		vo.setDataUltimaAlteracao(new Date());		
				
		Cliente entity = DozerConverter.parseObject(vo, Cliente.class);
		ClienteVO clienteVO = DozerConverter.parseObject(this.repository.save(entity), ClienteVO.class);		
		
		//Processo de envio de email
		JavaMail.enviar(vo.getContatos().iterator().next().getEmail());
				
		return clienteVO;
	}

	public Page<ClienteVO> findAll(Pageable pageable) {
		var page = repository.findAll(pageable);
		return page.map(this::convertToPersonVO);
	}

	public ClienteVO convertToPersonVO(Cliente vo) {
		return DozerConverter.parseObject(vo, ClienteVO.class);
	}
	
	public ClienteVO getById(Long id) {
		
		Optional<Cliente> op = this.repository.findById(id);
		
		if(op.isEmpty()) {
			return null;
		}
		
		return DozerConverter.parseObject(op.get(), ClienteVO.class);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

	public ClienteVO update(ClienteVO entity) {

		var cliente = repository.findById(entity.getId())
				.orElseThrow(() -> new ResourceNotFoundException("No record found for this ID"));
		
		boolean atualizar = false;

		if (Strings.isNotEmpty(entity.getNome()) && !cliente.getNome().equals(entity.getNome())) {
			cliente.setNome(entity.getNome());
			atualizar = true;
		}

		if (Strings.isNotBlank(entity.getNumeroDocumentoCPF())
				&& !cliente.getNumeroDocumentoCPF().equals(entity.getNumeroDocumentoCPF())) {
			cliente.setNumeroDocumentoCPF(entity.getNumeroDocumentoCPF());

			atualizar = atualizar == false ? true : atualizar;
		}
		
		if (Strings.isNotBlank(entity.getNumeroDocumentoCNPJ())
				&& !cliente.getNumeroDocumentoCNPJ().equals(entity.getNumeroDocumentoCNPJ())) {
			
			cliente.setNumeroDocumentoCNPJ(entity.getNumeroDocumentoCNPJ());

			atualizar = atualizar == false ? true : atualizar;
		}

		if (entity.getContatos() != null && !entity.getContatos().isEmpty()) {	
			atualizar = atualizar == false ? true : atualizar;
		}
		
		if(entity.getEndereco() != null) {
			atualizar = atualizar == false ? true : atualizar;
		}

		if (Strings.isNotEmpty(entity.getUrlSite()) && !cliente.getUrlSite().equals(entity.getUrlSite())) {
			cliente.setUrlSite(entity.getUrlSite());

			atualizar = atualizar == false ? true : atualizar;
		}		

		if (atualizar) {
			cliente.setDataUltimaAlteracao(new Date());
			cliente = this.repository.save(cliente);
		}

		return DozerConverter.parseObject(cliente, ClienteVO.class);
	}
}
