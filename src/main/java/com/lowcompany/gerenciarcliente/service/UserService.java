package com.lowcompany.gerenciarcliente.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.lowcompany.gerenciarcliente.entity.User;
import com.lowcompany.gerenciarcliente.repository.UserRepositoryImp;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepositoryImp repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = repository.findByUserName(username);

		if (user != null) {
			return user;
		} else {
			throw new UsernameNotFoundException("Username " + username + "not found");
		}		
	}
}
