package com.lowcompany.gerenciarcliente.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.lowcompany.gerenciarcliente.exception.ResourceNotFoundException;

public class ManipuladorProperties {

	public static Properties getProp() {
		Properties props = new Properties();
		
		try {
			FileInputStream file = new FileInputStream("src/main/resources/properties/data.properties");
			props.load(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(e.getCause().getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return props;
	}
}
