package com.lowcompany.gerenciarcliente.util;

import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.lowcompany.gerenciarcliente.exception.EmailException;

public class JavaMail {

	public static void enviar(String destinatario) {

		Properties dados = ManipuladorProperties.getProp();

		Properties props = new Properties();
		/** Parâmetros de conexão com servidor Gmail */
		props.put("mail.smtp.host", dados.getProperty("prop.mail.smtp.host"));
		props.put("mail.smtp.socketFactory.port", dados.getProperty("prop.mail.smtp.socketFactory.port"));
		props.put("mail.smtp.socketFactory.class", dados.getProperty("prop.mail.smtp.socketFactory.class"));
		props.put("mail.smtp.auth", dados.getProperty("prop.mail.smtp.auth"));
		props.put("mail.smtp.port", dados.getProperty("prop.mail.smtp.port"));

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(dados.getProperty("prop.email.remetente"),
						dados.getProperty("prop.email.senha"));
			}
		});

		/** Ativa Debug para sessão */
		session.setDebug(true);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(dados.getProperty("prop.email.remetente"))); // Remetente

			Address[] toUser = InternetAddress.parse(destinatario);// Destinatário(s)

			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject(dados.getProperty("prop.mail.message.subject"));// Assunto
			message.setText(dados.getProperty("prop.mail.message.body")); // Body
			/** Método para enviar a mensagem criada */
			Transport.send(message);

		} catch (MessagingException e) {
			throw new EmailException(e.getMessage());
		}
	}
}
