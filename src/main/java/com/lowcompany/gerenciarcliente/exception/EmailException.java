package com.lowcompany.gerenciarcliente.exception;

public class EmailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmailException(String exception) {
		super(exception);
	}
}
