package com.lowcompany.gerenciarcliente.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lowcompany.gerenciarcliente.entity.Cliente;

/**
 * @author Alex Sandro
 * @version 1.0
 *
 */
@Repository
public interface ClienteRepositoryImpl extends JpaRepository<Cliente, Long> {

	@Query("SELECT c FROM Cliente c WHERE c.nome LIKE :nome%")
	public Cliente findByName(@Param("nome") String nome);	
}
