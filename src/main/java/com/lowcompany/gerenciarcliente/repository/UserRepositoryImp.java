package com.lowcompany.gerenciarcliente.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lowcompany.gerenciarcliente.entity.User;

/**
 * @author Alex
 * @version 1.0
 *
 */
@Repository
public interface UserRepositoryImp extends JpaRepository<User, Long> {

	@Query("SELECT u FROM User u WHERE u.userName = :username")
	User findByUserName(@Param("username") String username);
}
